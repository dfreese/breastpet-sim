# Sherlock Usage

A quick summary of what I have learned from sherlock, as of this evening.

A job is submitted by

```
sbatch
```

An array job, which submits the same job many times, with some environmental
variables updated is run by:

```
sbatch --array=0-63
```

This will start jobs with SLURM_ARRAY_TASK_ID as 0 through 63, but the same job
id.  A useful resource on this can be
[found here](https://slurm.schedmd.com/job_array.html):


To see the status of the jobs of a particular user, you can use:
```
squeue -u dfreese
```


To probe how the jobs finished up, use the sacct program.  A particularly
useful result was elapsed and MaxRSS.  These show the cpu time and max memory
used by your job.
```
sacct
sacct --format="JobID,elapsed,Timelimit,MaxRSS,ReqMem,MaxVMSize,ncpus,ExitCode"
```
The longer format was described [here](https://www.accre.vanderbilt.edu/?page_id=2154#sinfo)


After submitting an array job, it can be necessary to do some work to combine the resulting outputs.  You can have a separate job to do this by specifying a dependency on the job array.

```
sbatch --depend=afterok:20882846 cleanup.sh
```

This launches cleanup.sh after all of the tasks in 20882846 complete successfully.  This was taken from the [slurm docs here](https://slurm.schedmd.com/job_array.html)
