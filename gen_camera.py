#!/usr/bin/env python

import miil
import numpy as np
import sys

outputfile = 'breast_panel_camera.dff'

system_shape = (2, 3, 8, 16, 2, 64)

# Based on DR-003946 by Agile Technologies.  Does not include the reflector in
# the size.
size = np.array((0.9, 0.9, 0.93))

positions = miil.get_position_global_crystal(
        range(miil.no_crystals(system_shape)))

p0_min = positions[:miil.no_crystals_per_panel(system_shape)].min(0)
p0_max = positions[:miil.no_crystals_per_panel(system_shape)].max(0)
p1_min = positions[miil.no_crystals_per_panel(system_shape):].min(0)
p1_max = positions[miil.no_crystals_per_panel(system_shape):].max(0)

# The alumina pieces extend 6mm off of the back of the modules and 1.25mm in
# front of the modules toward the FOV.  These numbers are approximate from
# Derek.  Seeing as we're using one slab of alumina to mimick the mechanical
# structure, I don't think we're interested in complete accuracy.  Forgot about
# the crystal size in the center calculations, but that cancels out.
alumina_p0_y_center = ((p0_min[1] - 6.0) + (p1_max[1] + 1.25)) / 2.0
alumina_p1_y_center = ((p1_max[1] + 6.0) + (p1_min[1] - 1.25)) / 2.0
alumina_size = np.array((
    p1_max[0] - p1_min[0] + size[0] + 1.25,
    (p1_max[1] + 6.0) - (p1_min[1] - 1.25) + size[1],
    p1_max[2] - p1_min[2] + size[2] + 1.25,
))

# Gray is in cm
size /= 10.0
positions /= 10.0
alumina_p0_y_center /= 10.0
alumina_p1_y_center /= 10.0
alumina_size /= 10.0

with open(outputfile, 'w') as output:
    output.write('# All of the LYSO crystals for the Breast Panel System\n')
    output.write('# system_shape = ({})\n'.format(
        ', '.join(str(i) for i in system_shape)))

    output.write('m Alumina\n')
    output.write('k 0.0 {0:0.3f} 0.0 {1:0.3f} {2:0.3f} {3:0.3f}\n'.format(
        alumina_p0_y_center, *alumina_size))
    output.write('k 0.0 {0:0.3f} 0.0 {1:0.3f} {2:0.3f} {3:0.3f}\n'.format(
        alumina_p1_y_center, *alumina_size))

    output.write('m LYSO\n')
    for idx in range(positions.shape[0]):
        pos = positions[idx, :]
        output.write(
            'k {0:0.3f} {1:0.3f} {2:0.3f} {3:0.3f} {4:0.3f} {5:0.3f}\n'.format(
            pos[0], pos[1], pos[2], *size))
