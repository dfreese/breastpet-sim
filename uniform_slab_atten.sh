#!/bin/bash
#
#SBATCH --job-name=test
#
#SBATCH --time=2:00:00
#SBATCH --ntasks=1
#SBATCH --array=0-499
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=1G

gray="$HOME/Gray/bin/gray"
scene="$HOME/breastpet-sim/uniform_slab_atten.dff"
phys="$HOME/breastpet-sim/GrayPhysics.json"
sim_time=3400000
seed=5
output="uniform_slab_atten.coinc.dat"

rank=$SLURM_ARRAY_TASK_ID
world=$(($SLURM_ARRAY_TASK_MAX + 1))

# srun hostname
cmd="$gray --seed $seed -f $scene --phys $phys -t $sim_time -r $rank -w $world -c ${SCRATCH}/${output}.${rank}"
echo $cmd
srun $cmd
