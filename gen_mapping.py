#!/usr/bin/env python
import sys
import miil
import gray
import numpy as np

outputfile = 'breast_panel.map'

system_shape = (2, 3, 8, 16, 2, 64)
system_shape_pcdrm = (2, 3, 4, 8, 4, 2, 64)
crystal_size = (0.9, 0.9, 0.93)

map_dtype = np.dtype([
    ('panel', int),
    ('cartridge', int),
    ('fin', int),
    ('module', int),
    ('daq', int),
    ('rena', int),
    ('apd', int),
    ('crystal', int),
    ('bx', int),
    ('by', int),
    ('bz', int),
    ])

data = np.empty(miil.no_crystals(system_shape), dtype=map_dtype)

data['crystal'] = np.arange(miil.no_crystals(system_shape))
data['apd'] = data['crystal'] // miil.no_crystals_per_apd(system_shape)
data['module'] = data['apd'] // miil.no_apds_per_module(system_shape)
data['fin'] = data['module'] // miil.no_modules_per_fin(system_shape)
data['cartridge'] = data['fin'] // miil.no_fins_per_cartridge(system_shape)
data['panel'] = data['cartridge'] // miil.no_cartridges_per_panel(system_shape)

loc_crystal = data['crystal'] % miil.no_crystals_per_apd(system_shape)
loc_module = data['module'] % miil.no_modules_per_fin(system_shape)
loc_fin = data['fin'] % miil.no_fins_per_cartridge(system_shape)
loc_cartridge = data['cartridge'] % miil.no_cartridges_per_panel(system_shape)

data['bx'] = loc_crystal // 8
data['by'] = loc_crystal % 8
data['bz'] = loc_crystal // 64

loc_daq, loc_rena, loc_rena_local_module = miil.pcfm_to_pcdrm(
        data['panel'], loc_cartridge, loc_fin, loc_module,
        system_shape_pcfm=system_shape,
        system_shape_pcdrm=system_shape_pcdrm)
data['daq'] = data['cartridge'] * system_shape_pcdrm[2] + loc_daq
data['rena'] = data['daq'] * system_shape_pcdrm[3] + loc_rena

gray.save_mapping_file(outputfile, data)
